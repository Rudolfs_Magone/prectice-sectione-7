import Vue from 'vue'
import App from './App.vue'
import ServerStatus from './components/ServerStatus.vue'
import Header from './components/Header.vue'
import ManageServer from './components/ManageServers.vue'


Vue.component('server-status',ServerStatus);
Vue.component('app-header',Header);
Vue.component('manage-servers', ManageServer)


new Vue({
  el: '#app',
  render: h => h(App)
})
